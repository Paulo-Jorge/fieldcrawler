#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Start all the sensors and manage measuarments data flow
"""

import requests, datetime, time
from time import gmtime, strftime

from sensors import sensor1
from sensors import sensor2

PORT1 = "/dev/ttyUSB0"
DATALAKE_HOST = "http://cehum.ilch.uminho.pt/datalake/api/sensors"

# EVENTS PY-RQ URL
#DATALAKE_HOST = "http://miradascruzadas.ilch.uminho.pt/api/datalake/"#sensor1 / sensor2 POST

TAGS = "janela fechada, ac off"
CLASSIFICATION = "0"#0=normal / 1 = anomalia fumo

class FieldCrawler:

    def __init__(self):
        
        self.readInterval = 10#seconds
        
        self.sensor1 = None
        self.sensor2 = None
        
        self.sensorsStart()
        self.startListen()
        
    def sensorsStart(self):
        #self.sensor1 = SDS011(port=self.port1,use_database=False)
        try:
            self.sensor1 = sensor1.Sensor1(PORT1)
            self.sensor2 = sensor2.Sensor2()
        except:
            print("Sensors start failed, check ports or if active")
            print("Trying to start again")
            self.sensorsStart()
        
    def startListen(self):
        print("Monitoring Sensors...")
        while True:
            dataDict = {'sensor':'sds011',
                    'carId': 'VP-35-44',
                    'carLocation': '41.5608 -8.3968',
                    'timeValue': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
                    'tags': TAGS,
                    'classification': CLASSIFICATION
                    }
            try:
                s1 = self.readSensor1()
                dataDict.update(s1)
            except:
                print("Sensor1 read failed")
            try:
                s2 = self.readSensor2()
                dataDict.update(s2)
            except:
                print("Sensor2 read failed")
                
            try:
                self.notifyDataLake(dataDict)
            except:
                print("Notify DataLake failed")
            self.notifyMonitorAlert(dataDict)
            
            time.sleep(self.readInterval)

    def readSensor1(self):
        measurement = self.sensor1.getMeasurement()
        dataDict = {'pm25': measurement["pm2.5"],
                    'pm10': measurement["pm10"]
                    }
        print(dataDict)
        return dataDict
        #try:
        #    self.notifyDataLake("sensor1", dataDict)
        #except:
        #    print("Notify DataLake failed")
        #self.notifyMonitorAlert("sensor1", dataDict)
        
    def readSensor2(self):
        measurement = self.sensor2.getMeasurement()
        dataDict = {'temperature': measurement["temperature"],
                    'gas': measurement["gas"],
                    'humidity': measurement["humidity"],
                    'pressure': measurement["pressure"],
                    'altitude': measurement["altitude"]
                    }
        print(dataDict)
        return dataDict
        #try:
        #    self.notifyDataLake("sensor2", dataDict)
        #except:
        #    print("Notify DataLake failed")
        #self.notifyMonitorAlert("sensor2", dataDict)
        
    def notifyDataLake(self, dataDict):
        res = requests.post(DATALAKE_HOST, json=dataDict)
        print('response from server:'+res.text)
        #dictFromResponse = res.json()
        #print(dictFromResponse)

    def notifyMonitorAlert(self, dataDict):
        ### CALL MONITOR ALERT (WITH PYTHON-RQ EVENTS MANAGER p.e.)
        pass
    
    def data2Json(self):
        pass

if __name__ == '__main__':
    main = FieldCrawler()
